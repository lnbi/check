/*
 * =====================================================================
 *
 *       Filename:  @(#)check.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Sun Jun 29 09:12:05 2014
 *        Changed:  < >
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  xwhuang
 *          Email:  huangxiaoweigx@gmail.com
 *
 * =====================================================================
 */

#ifndef __check_h__
#define __check_h__

//define check type
#define WC_NORMAL        0      /**< 正常 */
#define WC_ABSEND        1      /**< 旷工 */
#define WC_WORKLATE      2      /**< 迟到 */
#define WC_LEAVEEARY     3      /**< 早退 */
#define WC_WLADNLE       4      /**< 迟到and早退 */
#define WC_PUNCHABNORMAL 5      /**< 刷卡异常 */

char *ctype[] = {
    "NORMAL",
    "ABSEND",
    "WORKLATE",
    "LEAVEEARY",
    "WORK LATE & LEAVEEARY",
    "PUNCH ABNORMAL",
    NULL
};

struct wtime {
    unsigned int h;
    unsigned int m;
};

struct check {
    unsigned int   id;          /**< 员工的工号 */
    unsigned short check_type;  /**< 每条考勤记录的状态 */
    struct wtime start;     /**< 首次刷卡时间 */
    struct wtime end;     /**< 最后刷卡时间 */
    unsigned int work_time;     /**< min */
    struct check *next;
};

/*<
  o work time:
    09:00 ~ 17:30

  o record format:
    id mm:hh mm:hh ...

 >*/

#define DEFAULT_RECORD_FILE "check.r"

#endif  /**< end check.h */
