/*
 * =====================================================================
 *
 *       Filename:  @(#)check.c
 *
 *    Description:  2014年度技能鉴定考试实现
 *
 *        Version:  1.0
 *        Created:  Sun Jun 29 09:10:06 2014
 *        Changed:  < >
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  xwhuang
 *          Email:  huangxiaoweigx@gmail.com
 *
 * =====================================================================
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "check.h"

void usage()
{
    printf("0 \n  START: clear all CHECK record\n\n");
    printf("1,id[,mm:hh] \n  CHECK: do check\n\n");
    printf("2 \n  LIST: list all record\n\n");
    printf("3 \n  QUIT: quit the command line\n\n");
    printf("4 \n  LOAD: load the record file again\n\n");
}

void free_records(struct check **head) 
{
    struct check *curr = *head;
    struct check *item = NULL;
    
    if (head == NULL) {
        return;
    }

    while(curr) {
        item = curr;
        curr = curr->next;
        free(item);
    }
    *head = NULL;
}

int parse_wtime(const char *str, struct wtime *t)
{
    //mm:hh
    int i = 0;
    int len = strlen(str);
    int get_h = 1;

    t->h = 0;
    t->m = 0;

    while (i < len) {
        if (str[i] == ':') {
            get_h = 0;
            i++;
            continue;
        }
        if (str[i] == 0xa) {
            return 0;
        }
        if (str[i] > '9' || str[i] < '0') {
            return -1;
        }
        if (get_h) {
            t->h = t->h * 10 + str[i] - '0';
            if (t->h > 23) {
                return -1;
            }
        } else {
            t->m = t->m * 10 + str[i] - '0';
            if (t->m > 59) {
                return -1;
            }
        }
        i++;
    }
    
    return 0;
}

static void do_check(struct check *c)
{
    //no record time
    if ((c->start.h == 0) && (c->start.m == 0) && 
        (c->end.h == 0) && (c->end.m == 0)) {
        c->check_type = WC_ABSEND;
        c->work_time = 0;
        return;
    }

    //only one record time
    if ((c->end.h == 0) && (c->end.m == 0)) {
        c->check_type = WC_PUNCHABNORMAL;
        c->work_time = 0;
        return;        
    }

    //two record time
    if ((c->start.h > 17) || (c->start.h == 17 && c->start.m >=30)) {
        c->check_type = WC_ABSEND;
        c->work_time = 0;
        return;
    }

    if ((c->start.h >= 9) && (c->start.m > 0)) {//迟到
        if ((c->end.h > 17) || (c->end.h == 17 && c->end.m >= 30)) {
            c->check_type = WC_WORKLATE;
        } else {
            c->check_type = WC_WLADNLE;            
        }
        c->work_time = c->end.h * 60 + c->end.m - 
            (c->start.h * 60 + c->start.m);            
        return;
    } else {
        if ((c->end.h > 17) || (c->end.h == 17 && c->end.m >= 30)) {
            c->check_type = WC_NORMAL;
        } else {
            c->check_type = WC_LEAVEEARY;
        }
        c->work_time = c->end.h * 60 + c->end.m - 
            (c->start.h * 60 + c->start.m);            
        return;        
    }
}

static void
insert_wtime(struct check *c, struct wtime *t)
{
    if (c->start.h == 0 && c->start.m ==0) {
        c->start.h = t->h;
        c->start.m = t->m;
        return;
    }
    if (c->end.h == 0 && c->end.m == 0) {
        c->end.h = t->h;
        c->end.m = t->m;                
        return;
    }
    if ((t->h * 60 + t->m) > 
        (c->end.h * 60 + c->end.m)) {
        c->end.h = t->h;
        c->end.m = t->m;                
    } else if ((t->h * 60 + t->m) < 
               (c->start.h * 60 + c->start.m)) {
        c->start.h = t->h;
        c->start.m = t->m;                
    }
}

static int 
init(const char *cfile, struct check **head)
{
    int i = 0;
    char record[1024] = {0};
    long int rlen = 0;
    unsigned int id = 0;
    struct check *tmpc = NULL;
    struct check *curr = NULL; 
    struct check *item = NULL;
    int newitem = 0;
    FILE *fp = fopen(cfile, "r");

    if (fp == NULL) {
        *head = NULL;
        return -1;
    }

    while (fgets(record, 1023, fp) != NULL) {
        item = NULL;
        newitem = 0;
        //get record id
        char *token = strtok(record, ",");
        if (token == NULL) {
            free_records(head);
            *head = NULL;
            return -1;
        }

        if ((id = atoi(token)) == 0) {//invalid id
            free_records(head);
            *head = NULL;
            return -1;
        }

        tmpc = *head;
        while (tmpc) {
            if (tmpc->id == id) {
                item = tmpc;
                break;
            } else {
                tmpc = tmpc->next;
            }
        }

        if (item == NULL) {
            item = malloc(sizeof(struct check));
            if (item == NULL) {
                free_records(head);
                perror("malloc");
                *head = NULL;
                return -1;
            }
            memset(item, 0, sizeof(struct check));
            item->id = id;
            newitem = 1;
        }

        //get time
        struct wtime tmpt;
        token = strtok(NULL, ",");
        while (token != NULL) {
            if (parse_wtime(token, &tmpt) != 0) {
                printf("parse_wtime:%s\n", token);
                free_records(head);
                free(item);
                return -1;
            }
            insert_wtime(item, &tmpt);
            token = strtok(NULL, ",");
        }
        
        //get check type and work time
        do_check(item);

        if (*head == NULL) {
            *head = item;
            curr = item;
        } else {
            if (newitem) {
                curr->next = item;
                curr = item;
            }
        }
        memset(record, 1024, 0);
    };
    
    return 0;
}

void show(struct check *head)
{
    struct check *item = head;

    while (item) {
        printf("ID=%d TYPE = %s " 
               "CHECK IN=%02d:%02d CHECK OUT=%02d:%02d "
               "WORK TIME=%02dH:%02dM\n",
               item->id, ctype[item->check_type],
               item->start.h,item->start.m,
               item->end.h,item->end.m,
               item->work_time/60, item->work_time%60);
        item = item->next;
    }
}

int main(int argc, char *argv[])
{
    unsigned char choice;
    unsigned char cmd[256];
    struct check *list = NULL;
    char *file = "check.r";

    if (argc > 2) {
        printf("Usage:%s [record file]\n", argv[0]);
        exit(-1);
    }

    if (argc == 2) {
        printf("We will read record file: %s\n", argv[1]);
        if (init(argv[1], &list) != 0) {
            perror("init()");
            exit(-1);
        }        
        file = argv[1];
    } else {
        printf("We will read defualt record file: %s\n", "check.r");
        if (init("check.r", &list) != 0) {
            perror("init()");
            exit(-1);
        }
    }

    printf("Welcom to CHECK system, type '?' for help.\n\n");

    while (1) {
        printf("CHECK > ");
        memset(cmd, 0, 256);
        fgets(cmd, 255, stdin);

        choice = cmd[0];

        switch (choice) {
        case '\n':
        case '\r':
        {
            continue;
        }
        case '0':
        {
            if (strlen(cmd) != 2) {
                usage();
                continue;
            }
            char scmd[1024] = {0};
            free_records(&list);
            printf("OK\n");
            //todo
            break;
        }
        case '1':
        {
            int eid;
            struct wtime t;
            char *token;
            token = strtok(cmd, ",");

            //get eid
            token = strtok(NULL, ",");
            if (token == NULL) {
                printf("USAGE:1,<eid>[,checktime]\n");
                break;
            }
            char *errp;
            eid = strtol(token, &errp, 10);
            if (eid > 10000 || eid <= 0 || *errp != 0xa) {
                printf("ERROR:ID INVLAID\n");
                break;
            }

            //insert
            struct check *item = list;
            while (item) {
                if (item->id == eid) {
                    break;
                }
                item = item->next;
            }

            if (item == NULL) {
                item = malloc(sizeof(struct check));
                if (item == NULL) {
                    perror("malloc");
                    break;
                }
                memset(item, 0, sizeof(struct check));
                item->id = eid;
                if (list == NULL) {
                    list = item;
                } else {
                    item->next = list;
                    list = item;
                }
            }

            //get time
            token = strtok(NULL, ",");printf("=%s=\n", token);
            if (token == NULL) {
                printf("OK\n");
                break;
            } else {
                if (parse_wtime(token, &t) == -1) {
                    printf("ERROR:TIME INVLAID\n");
                    break;
                } else {
                    insert_wtime(item, &t);
                }
                do_check(item);
                printf("OK\n");
                char checktofilecmd[1024] = {0};
                sprintf(checktofilecmd, "echo \"%u,%02d:%02d\" >> %s",
                        item->id,t.h,t.m,file);
                system(checktofilecmd);
            }
            //toto
            break;
        }
        case '2':
        {
            if (strlen(cmd) != 2) {
                usage();
                continue;
            }
            if (list) {
                show(list);
            }
            break;
        }
        case '3':
        {
            if (strlen(cmd) != 2) {
                usage();
                continue;
            }
            printf("BYE!\n");
            exit(0);
            break;
        }
        case '4':
        {
            if (strlen(cmd) != 2) {
                usage();
                continue;
            }

            if(init(file, &list) != 0) {
                perror("load");                
            }

            break;
        }
        case '?':
        {
            usage();
            break;
        }
        defualt:
        {
            usage();
        }
        }
    }

    return 0;
}
